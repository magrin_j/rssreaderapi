// server.js

// BASE SETUP
// ===================================================

// Load the package we need
var express             = require('express');
var fs                  = require('fs');
var app                 = express();
var bodyParser          = require('body-parser');
var mongoose            = require('mongoose');
var session             = require('express-session');
var cookieParser        = require('cookie-parser');
var passport            = require('./app/config/auth.js');

// Set out port
var port        = process.env.PORT || 3000;
var dbURL       = 'mongodb://localhost/rssreader';

// Configure app to user body-parser
app.set('title', 'RSSReaderAPI');

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({secret: 'RSSr3adEr2ecr3tK3?',
                 saveUninitialized: true,
                 resave: true}));
app.use(passport.initialize());
app.use(passport.session());

mongoose.connect(dbURL);

mongoose.connection.on('connected', function() {
    console.log('Connected to local mongoose database : ' + dbURL);
});

mongoose.connection.on('error', function(err) {
    console.log('Mongoose default connection error : ' + err);
});

mongoose.connection.on('disconnected', function() {
    console.log('Mongoose default connection disconnected');
});

process.on('SIGINT', function() {
    mongoose.connection.close(function() {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

// MODELS FOR OUR API
// ===================================================
// Bootstrap models
var router      = express.Router();
var routes_path = __dirname + '/app/routes';
var models_path = __dirname + '/app/models';

// route middleware that will happen on every request
router.use(function(req, res, next) {

	// log each request to the console
	console.log(req.method, req.url);

	// continue doing what we were doing and go to the route
	next();	
});

var loadModels = function(path) {
    fs.readdirSync(path).forEach(function(file) {
        var newPath = path + '/' + file;
        var stat = fs.statSync(newPath);

        if (stat.isFile()) {
            if (/(.*)\.(js$|coffee$)/.test(file)) {
                require(newPath);
            }
        } else if (stat.isDirectory()) {
            loadModels(newPath);
        }
    });
};
loadModels(models_path);

// ROUTES FOR OUR API
// ===================================================
var loadRoutes = function(path) {
    fs.readdirSync(path).forEach(function(file) {
        var newPath = path + '/' + file;
        var stat = fs.statSync(newPath);

        if (stat.isFile()) {
            if (/(.*)\.(js$|coffee$)/.test(file)) {
                require(newPath)(router, passport);
            }
        } else if (stat.isDirectory()) {
            loadRoutes(newPath);
        }
    });
};
loadRoutes(routes_path);

app.use('/api', router);
// START THE SERVER
// ===================================================
app.listen(port);
console.log("Server is runing on port : " + port);