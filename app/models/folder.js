/*
 * Folder model object
 *
 */

'use strict';

var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;

var FolderSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    _feeds: [{
        type: Schema.ObjectId,
        ref: 'Feed'
    }],
    _owner: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('Folder', FolderSchema);
