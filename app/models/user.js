/*
 * User model object
 *
 */

'use strict';

var mongoose        = require('mongoose');
var validator       = require('validator');
var emailExist      = require('email-existence');
var bcrypt          = require('bcrypt-nodejs');
var Schema          = mongoose.Schema;

var UserSchema = new Schema({
    email: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    username: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    password: {
        type: String,
        trim: true,
        required: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now
    },
    _feedfolders: [{
        type: Schema.ObjectId,
        ref: 'Folder'
    }]
});

UserSchema.pre('save', function(done) {
    var user = this;
    
    if (!user.isModified('email'))
        return done();
    
    if (!validator.isEmail(user.email))
        return done(new Error('Invalid email adress.'));
    /*
    emailExist.check(user.email, function(err, res) {
        if (!res)
            return done(new Error('This email adress is not reatchable.'));
        done();
    });*/
    done();
});

UserSchema.pre('save', function(done) {
    var user = this;

    if (!user.isModified('password'))
        return done();

    bcrypt.genSalt(5, function(err, salt) {
        
        if (err)
            return done(err);

        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if (err)
                return done(err);

            user.password = hash;
            done();
        });
    });
});

UserSchema.methods.verifyPassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err)
            return cb(err);
        
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', UserSchema);