/*
 * Feed model object
 *
 */

'use strict';

var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;

var FeedSchema = new Schema({
    name: {
        type: String,
        trim: true,
        require: true
    },
    url: {
        type: String,
        trim: true,
        required: true
    },
    icon: {
        type: String,
        require: true
    }, 
    date: {
        type: Date,
        default: Date.now
    },
    _folderowner: {
        type: Schema.ObjectId,
        ref: 'Folder'
    }
});

module.exports = mongoose.model('Feed', FeedSchema);