/*
 * Controllers for folder object
 *
 */

'use strict';

var mongoose        = require('mongoose');
var Folder          = mongoose.model('Folder');
var Feed          = mongoose.model('Feed');
var User            = mongoose.model('User');

exports.create = function(req, res) {
    if (req.body.name === undefined || req.body.name.length < 1) {
        return res.json({ status: 'NOK', message: 'Invalid folder name.' });
    } else {
        Folder.findOne( { name: req.body.name, _owner: req.user._id }, function(err, folder) {
            if (err) {
                return res.json({ status: "NOK", message: err.message });
            }
            if (folder) {
                return res.json({ status: 'NOK', message: 'This folder already exist.' });
            } else {

                var new_folder = new Folder({
                    name: req.body.name,
                    _owner: req.user.id
                });

                new_folder.save(function(err) {
                    if (err) {
                        return res.json({ status: 'NOK', message: err.message });
                    }

                    User.findByIdAndUpdate(req.user._id, {$push: {_feedfolders: new_folder._id}}).exec(function(err) {
                        if (err)
                            return res.json({ status: 'NOK', message: err.message });

                        res.json({ status: 'OK', message: new_folder });
                    });
                });
            }
        });
    }
}

exports.update = function(req, res) {
    if (req.user._feedfolders.indexOf(req.params.folderId) == -1) {
        return res.json({ status: 'NOK', message: 'Invalid folder id.' });
    }
    if (req.body.name.length < 1) {
        return res.json({ status: 'NOK', message: 'The foldername can\'t be null.' });
    }
    Folder.findByIdAndUpdate(req.folderObj._id, { name: req.body.name }, function(err, update_folder) {
        if (err)
            return res.json({ status: 'NOK', message: err.message });

        res.json({ status: 'OK', message: update_folder });
    });
}

exports.delete = function(req, res) {
    if (req.user._feedfolders.indexOf(req.params.folderId) == -1)
        return res.json({ status: 'NOK', message: 'Invalid folder id.' });
    Folder.findById(req.folderObj._id, function(err, folder) {
        if (err) {
            return res.json({ status: "NOK", message: err.message });
        }
        if (!folder) {
            return res.json({ status: 'NOK', message: 'This folder doesn\'t exist' });
        } else {
            if (folder._owner !== undefined)
                folder._owner = undefined;

            User.update({ _id: req.user._id }, { $pull: { '_feedfolders': req.folderObj._id } }, function(err) {
                if (err)
                    return res.json({ status: 'NOK', message: 'Can\'t unset folder of user.' });

                Feed.remove({ _folderowner: req.folderObj._id }, function(err) {
                    if (err) {
                        return res.json({ status: 'NOK', message: 'Can\'t remove feeds in folder.' });
                    }

                    Folder.remove({ _id: req.folderObj._id }, function(err) {
                        if (err) {
                            return res.json({ status: 'NOK', message: 'Can\'t remove folder.' });
                        } else {
                            res.json({ status: 'OK', message: 'Folder successfully removed.' });
                        }
                    });
                });
            });
        }
    });
}

exports.getUserFolder = function(req, res) {
    Folder.find({ _owner: req.user._id })
    .populate('_feeds')
    .sort('name')
    .exec(function(err, folders) {
        if (err)
            return res.json({ status: 'NOK', message: err.message });

        res.json({ status: 'OK', message: folders });
    });
}

exports.ownByUser = function(req, res, next, folderId) {
    if (folderId == undefined) {
        return res.json({ status: 'NOK', message: 'Folder id isn\'t set.' });
    }
    Folder.findOne({ _id: folderId }, function(err, folder){
        if (err) {
            return res.json({ status: 'NOK', message: err.message });
        }

        if (!folder) {
            return res.json({ status: 'NOK', message: 'Folder not found.' });
        }

        req.folderObj = folder;
        next();
    });
}
