/*
 * Controllers for feed object
 *
 */

'use strict';

var mongoose        = require('mongoose');
var gfeed           = require('google-feed-api');
var validator       = require('validator');
var url             = require("url");
var Feed            = mongoose.model('Feed');
var Folder          = mongoose.model('Folder');

exports.getFeedObj = function(req, res, next, feedId) {
    if (feedId == undefined) {
        return res.json({ status: 'NOK', message: 'Feed id isn\'t set.' });
    }
    Feed.findById(feedId, function(err, feed) {
        if (err) {
            return res.json({ status: 'NOK', message: err.message });
        }

        if (!feed) {
            return res.json({ status: 'NOK', message: 'Feed not found.' });
        }

        req.feedObj = feed;
        next();
    });
}

exports.getFeedData = function(req, res) {
    var parseFeed = new gfeed.Feed(req.feedObj.url);

    parseFeed.setNumEntries(20);
    parseFeed.includeHistoricalEntries();
    parseFeed.listItems(function(items) {
        res.json({ status: 'OK', messages: items });
    });
}

exports.add = function(req, res) {
    if (req.user._feedfolders.indexOf(req.params.folderId) == -1)
        return res.json({ status: 'NOK', message: 'Invalid folder id.' });
    if (req.body.url === undefined || req.body.name === undefined ||
       req.body.url.length < 1 || req.body.name < 1) {
        return res.json({ status: 'NOK', message: 'Invalid post feed command.' });
    } else {
        Feed.findOne( { url: req.body.url, _folderowner: req.folderObj._id }, function(err, feed) {
            // Check if database error
            if (feed) {
                return res.json({ status: 'NOK', message: 'This feed already exist in this folder.' });
            } else {

                if (!validator.isURL(req.body.url)) {
                    return res.json({ status: 'NOK', message: 'Invalid url adress.' });
                }

                var url_icon = "http://" + url.parse(req.body.url).host + "/favicon.ico";
                
                var new_feed = new Feed({
                    name: req.body.name,
                    url: req.body.url,
                    icon: url_icon,
                    _folderowner: req.folderObj._id
                });

                new_feed.save(function(err) {
                    if (err)
                        return res.json({ status: 'NOK', message: err.message });

                    Folder.findByIdAndUpdate(req.folderObj._id, {$push: {_feeds: new_feed._id}}).exec(function(err) {
                        if (err)
                            return res.json({ status: 'NOK', message: err.message });

                        res.json({ status: 'OK', message: new_feed });
                    });
                });
            }
        });
    }
}

exports.update = function(req, res) {
    if (req.body.name === undefined && req.body.url === undefined) {
        return res.json({ status: 'NOK', message: 'Invalid update feed command.' });
    }
    var toUpdate = {}
    
    if (req.body.url !== undefined && req.body.url.length < 1) {
        toUpdate['url'] = req.body.url;
        toUpdate['icon'] = "http://" + url.parse(req.body.url).host + "/favicon.ico";
    }
    
    if (req.body.name !== undefined && req.body.name.length < 1) {
        toUpdate['name'] = req.body.name;
    }

    Feed.findByIdAndUpdate(req.feedObj._id, toUpdate, function(err, update_feed) {
        if (err)
            return res.json({ status: 'NOK', message: err.message });

        res.json({ status: 'OK', message: update_feed });
    });
}

exports.delete = function(req, res) {
    if (req.user._feedfolders.indexOf(req.params.folderId) == -1)
        return res.json({ status: 'NOK', message: 'Invalid folder id.' });
    Feed.findOne({ _id: req.feedObj._id, _folderowner: req.folderObj._id }, function(err, feed){
        if (err) {
            return res.json({ status: 'NOK', message: err.message });    
        }

        if (!feed) {
            return res.json({ status: 'NOK', message: 'This feed doesn\'t exist' });
        } else {
            if (feed._folderowner !== undefined)
                feed._folderowner = undefined;

            Folder.update({ _id: req.folderObj._id }, { $pull: { '_feeds': req.feedObj._id } }, function(err) {
                if (err)
                    return res.json({ status: 'NOK', message: 'Can\'t unset feed of folder.' });

                Feed.remove({ _id: req.feedObj._id }, function(err) {
                    if (err)
                        return res.json({ status: 'NOK', message: 'Can\'t remove feed.' });
                    else
                        res.json({ status: 'OK', message: 'Feed successfully removed.' });
                });
            });
        }
    });
}
