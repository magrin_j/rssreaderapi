/*
 * Controllers for user object
 *
 */

'use strict';

var mongoose        = require('mongoose');
var async           = require('async');
var crypto          = require('crypto');
var nodemailer      = require('nodemailer');
var User            = mongoose.model('User');

exports.logout = function(req, res) {
    req.logout();
    return res.json({ status: 'OK', message: 'Successfuly logout.' });
}

exports.login = function(req, res, next) {
    return res.json({ status: 'OK', message: req.user });
}

exports.delete = function(req, res, next) {
    User.remove({ email: req.user.email }, function(err) {
        if (err) {
            return res.json({ status: 'NOK', message: 'Can\'t remove this user.' });
        } else {
            req.logout();
            return res.json({ status: 'OK', message: 'Successfuly removed' });
        }
    });
}

exports.signup = function(req, res) {
    if (req.body.username === undefined || req.body.password === undefined ||
        req.body.email === undefined || req.body.username.length < 1 ||
        req.body.password.length < 1 || req.body.email.length < 1) {
        return res.json({ status: 'NOK', message: 'Invalid field.' });
    } else {
        User.findOne( { email: req.body.email }, function(err, user) {
            if (err) {
                switch (err.code) {
                    case 11000:
                    case 11001:
                        return res.json({ status: 'NOK', message: 'Username already taken.' });
                        break;
                    default:
                        return res.json({ status: 'NOK', message: err.message });
                }
            }
            if (user) {
                return res.json({ status: 'NOK', message: 'An user already exist with this email adress.' });
            } else {

                var new_user = new User({
                    email: req.body.email,
                    username: req.body.username,
                    password: req.body.password
                });

                new_user.save(function(err) {
                    if (err)
                        return res.json({ status: 'NOK', message: err.message });

                    res.json({ status: 'OK', message: new_user });
                });
            }
        });
    }
}

exports.update = function(req, res) {
    User.findById({ _id: req.user._id }, function(err, user) {
        if (err)
            return res.json({ status: 'NOK', message: err.message });

        if (req.body.username !== undefined && req.body.username.length > 0 &&
            req.body.username != req.user.username)
            user.username = req.body.username;
        if (req.body.password !== undefined && req.body.password.length > 0)
            user.password = req.body.password;

        user.save(function(err, update_user) {
            if (err) {
                switch (err.code) {
                    case 11000:
                    case 11001:
                        return res.json({ status: 'NOK', message: 'Username or email already taken.' });
                        break;
                    default:
                        return res.json({ status: 'NOK', message: err.message });
                }
            }

            res.json({ status: 'OK', message: update_user });
        });
    });
}

exports.resetPassword = function(req, res) {
    if (req.body.email === undefined || req.body.email.length < 1) {
        return res.json({ status: 'NOK', message: 'Invalid field.' });
    }
    async.waterfall([
        function(done) {
            crypto.randomBytes(10, function(err, buf) {
                var password = buf.toString('hex');
                done(err, password);
            });
        },
        function(password, done) {
            User.findOne({ email: req.body.email }, function(err, user) {
                if (!user)
                    return res.json({ status: 'NOK', message: 'Unknown email adress.' });

                user.password = password; // Set new password

                user.save(function(err) {
                    done(err, password, user);
                });
            });
        },
        function(password, user, done) {
            var smtpTransport = nodemailer.createTransport();
            var mailOptions = {
                from: 'no-reply@rssbell.magrin.fr',
                to: user.email,
                subject: 'Your password reset confirmation',
                text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                'A new password was generated for your account:\n\n' +
                'password: ' + password + '\n\n' +
                'If you did not request this, please ignore this email.\n'
            };
            smtpTransport.sendMail(mailOptions, function(err) {
                if (err)
                    return res.json({ status: 'NOK', message: err.message });
                return res.json({ status: 'OK', message: 'An e-mail has been sent to ' + user.email + ' with further instructions.' });
            });
        }
    ], function(err) {
        if (err)
            return res.json({ status: 'NOK', message: err.message });
    });
}
