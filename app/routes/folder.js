/*
 * Routes for folder object
 *
 */

'use strict';

// User routes use users controller
var folder              = require('../controllers/folder');

module.exports = function(app, passport) {
    
    app.param('folderId', folder.ownByUser);
        
    app.post('/folder', passport.isAuthenticated, folder.create);
    
    app.delete('/folder/:folderId', passport.isAuthenticated, folder.delete);
    
    app.put('/folder/:folderId', passport.isAuthenticated, folder.update);
    
    app.get('/folder', passport.isAuthenticated, folder.getUserFolder);
    
}