/*
 * Routes for user object
 *
 */

'use strict';

// User routes use users controller
var user            = require('../controllers/user');

module.exports = function(app, passport) {

    app.get('/user/login', passport.isAuthenticated, user.login);
    
    app.get('/user/logout', passport.isAuthenticated, user.logout);
    
    app.post('/user', user.signup);
    
    app.delete('/user', passport.isAuthenticated, user.delete);
    
    app.put('/user', passport.isAuthenticated, user.update);
        
    app.post('/user/forgot', user.resetPassword);
}
