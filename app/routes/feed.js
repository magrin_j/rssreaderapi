/*
 * Routes for feed object
 *
 */

'use strict';

// User routes use users controller
var feed                = require('../controllers/feed');
var folder              = require('../controllers/folder');

module.exports = function(app, passport) {
    
    app.param('folderId', folder.ownByUser);    
    
    app.param('feedId', feed.getFeedObj);
                
    app.post('/feed/:folderId', passport.isAuthenticated, feed.add);
    
    app.put('/feed/:feedId', passport.isAuthenticated, feed.update);
    
    app.delete('/feed/:folderId/:feedId', passport.isAuthenticated, feed.delete);
    
    app.get('/feed/:feedId', passport.isAuthenticated, feed.getFeedData);
    
}