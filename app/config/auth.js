/*
 * Controllers for user authentification of user
 *
 */

var passport            = require('passport');
var LocalStrategy       = require('passport-local').Strategy;
var BasicStrategy       = require('passport-http').BasicStrategy;
var User                = require('../models/user');

passport.use(new LocalStrategy(
    function(username, password, callback) {
        User.findOne({ username: username }, function (err, user) {
            if (err)
                return callback(err);

            if (!user)
                return callback(null, false, { message: 'Unknown user.' });

            user.verifyPassword(password, function(err, isMatch) {
                if (err)
                    return callback(err);

                if (!isMatch)
                    return callback(null, false, { message: 'Invalid password.' });

                return callback(null, user);
            });
        });
    }
));

passport.use(new BasicStrategy(
    function(username, password, callback) {
        User.findOne({ username: username }, function (err, user) {
            if (err)
                return callback(err);

            if (!user)
                return callback(null, false, { message: 'Unknown user.' });

            user.verifyPassword(password, function(err, isMatch) {
                if (err)
                    return callback(err);

                if (!isMatch)
                    return callback(null, false, { message: 'Invalid password.' });

                return callback(null, user);
            });
        });
    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

passport.isLogged = function(req, res, done) {
    if (!req.user)
        return res.json({ status: 'NOK', message: 'User isn\'t logged.' });
    return done();
};

passport.isAdmin = function(req, res, done) {
    if (!req.user)
        return res.json({ status: 'NOK', message: 'User isn\'t logged.' });
    if (req.user.isAdmin == false)
        return res.json({ status: 'NOK', message: 'You must be an admin user for see this part.' });
    return done();
};

passport.isAuthenticated = passport.authenticate('basic');

module.exports = passport;